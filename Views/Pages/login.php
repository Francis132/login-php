<?php

  include __DIR__ . '/../../Controller/loginController.php';

  if( isset($_POST['email']) && isset($_POST['pass']) ) {

    $user = new loginController( $_POST['email'], $_POST['pass'] );
    $username = $user->checkUser();

    if( $username != 'error' ) {

      if($username['password'] == $user->getPassword() ) {
          $_SESSION['user'] = $username;
          header('Location: http://localhost/login/?page=dashboard ');
      } else {
          $_SESSION['password-error'] = 'La contraseña es incorrecta';
      }
      
    }   
  }

  if( $_SESSION['user'] ) {
    header('Location: http://localhost/login/?page=dashboard ');
  }
  

?>

<div class="w-50 m-auto mt-5 border p-5 rounded shadow">
  <form method="POST">
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Email address</label>
      <input type="email" class="form-control" name="email" >
      <?php if( isset( $_SESSION['not-found-user'] ) ):  ?>
        <p class="text-danger"> <?= $_SESSION['not-found-user'] ?> </p>
      <?php endif; ?>
    </div>
    <div class="mb-3">
      <label for="exampleInputPassword1" class="form-label">Password</label>
      <input type="password" class="form-control" name="pass">
      <?php if( isset( $_SESSION['password-error'] ) ):  ?>
        <p class="text-danger"> <?= $_SESSION['password-error'] ?> </p>
      <?php endif; ?>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
